<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/foogra/templates/views/views-view-fields--product.html.twig */
class __TwigTemplate_ca147d66fb8a16a06ae29c4a582b523a450aea80706b34bb643806f243ce7608 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 32
        echo "


<div class=\"col-md-6\">
              <article class=\"blog\">
                <figure>
                  <a href=\"#\">";
        // line 38
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["fields"] ?? null), "field_product_image", [], "any", false, false, true, 38), "content", [], "any", false, false, true, 38), 38, $this->source), "html", null, true);
        echo "
                  </a>
                </figure>
                <div class=\"post_info\">
                  <h2><a href=\"#\">";
        // line 42
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["fields"] ?? null), "title", [], "any", false, false, true, 42), "content", [], "any", false, false, true, 42), 42, $this->source));
        echo "</a></h2>
                  <div class=\"row _brdr_top\">
                    <div class=\"col-md-6 _258sdh\">";
        // line 44
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["fields"] ?? null), "body", [], "any", false, false, true, 44), "content", [], "any", false, false, true, 44), 44, $this->source));
        echo "</div>
                    <div class=\"col-md-6 _24sdsd\">
                      <div class=\"menu_item\">
                                       
                                        <h4>Scan here on your mobile </h4>
                                        <p>To purchase this product on your app to avail exclusive app-only</p>
                                    </div>
                      <img class=\"img-fluid lazy loaded\" alt=\"barcode\" class=\"\" src=\"";
        // line 51
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["field_barcode"] ?? null), 51, $this->source), "html", null, true);
        echo "\">
                    </div>                    
                  </div>                       
                </div>
              </article>
              <!-- /article -->
</div>
";
    }

    public function getTemplateName()
    {
        return "themes/custom/foogra/templates/views/views-view-fields--product.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 51,  59 => 44,  54 => 42,  47 => 38,  39 => 32,);
    }

    public function getSourceContext()
    {
        return new Source("", "themes/custom/foogra/templates/views/views-view-fields--product.html.twig", "C:\\laragon\\www\\synergies\\web\\themes\\custom\\foogra\\templates\\views\\views-view-fields--product.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array();
        static $filters = array("escape" => 38, "raw" => 42);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                [],
                ['escape', 'raw'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
